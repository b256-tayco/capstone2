const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// Create/add product

module.exports.addProduct = (product) => {

	let newProduct = new Product({
		name: product.name,
		description: product.description,
		genre: product.genre,
		price: product.price

	});

	return newProduct.save().then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		};
	});
};


//Retrieve All Products

module.exports.getAllProducts = (query) => {
	return Product.find({ ...query }).then(result => result)
};

// Retrieve all active (inStock) products

module.exports.getAllInStock = () => {

	return Product.find({inStock: true}).then(result => result)
};


//Retrieve a product

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => result)
};


// Update a product

module.exports.updateProduct = (product, params) => {

	return Product.findByIdAndUpdate(
		params.productId, {...product}, {new: true}).then((result, err) => {

			if(err) {

				return err;

			} else {

				return result;
			}
		})
}

//Archive Product 

module.exports.archiveProduct = (reqParams) => {

	let updateStock = {
		inStock : false
	};

	return Product.findByIdAndUpdate(reqParams.productId,updateStock, { new: true }).then((product, err) => {

		if(err) {

			return err;

		} else {

			return product;

		}
	});
};


//Activate Product

module.exports.restockProduct = (reqParams) => {

	let restock = {
		inStock : true
	};

	return Product.findByIdAndUpdate(reqParams.productId,restock, { new: true }).then((product, err) => {

		if(err) {

			return err;

		} else {

			return product;

		}
	});
};