const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

//register user

module.exports.registerUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then((existingUser) =>{

		if(existingUser) {

			return  { success: false, message: "User already exists." };
		} else {

			let newUser = new User ({
				userName: requestBody.userName,
				email: requestBody.email,
				password: bcrypt.hashSync(requestBody.password, 10)
			});

			return newUser.save().then((user, err) => {

				if(err) {
					return false;

				} else {
					return true;
				}
			});
		}
	});
};

// authenticate user

module.exports.authenticateUser = (requestBody) => {
	return User.findOne({email:requestBody.email}).then(result => {

		if(result == null) {

			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect){

				return {access: auth.createAccessToken(result)}
			} else {

				return false;
			}
		}
	})
};


//create order



module.exports.createOrder = async (data) => {


 let totalAmount = 0;
 const ids = data.body.products.map(({product}) => product.productId);
 let products = [];
 const productInfo = await Product.find({_id:{$in:ids}});

 data.body.products.forEach(( { product, quantity } ) => {

	const item = productInfo.find(x => product.productId === x._id.toString());



	totalAmount += item.price * quantity;
	
     products.push({
     	productId: item._id,
     	productName: item.name,
     	quantity: quantity
     })
 });
	


const order = {
	products: products,
	totalAmount: totalAmount
}

 const user = await User.findById(data.userId);

  user.orderedProduct.push(order);
  user.markModified('orderedProduct');

try{
	await user.save();

    await Product.updateMany({_id:{$in:ids}}, {$push: {userOrders:{userId: user._id} }}) 
    return { status: "ok" };

} catch(error) {
	return error
}

}




//Retrieve User Details


module.exports.getUserDetails = () => {

	return User.find({}).then(result => result );

};


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		result.password = "";
		return result;

	});

};



//Set user as Admin

module.exports.setAsAdmin = (reqParams) => {

	let set = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.userId,set, { new: true }).then((newAdmin, err) => {

		if(err) {

			return err;

		} else {

			return newAdmin;

		}
	});
};


//Retrieve all orders

module.exports.getAllOrders = () => {

	return User.find({}).populate('orderedProduct').then(user => {

		const allOrders = user.map(user => user.orderedProduct);
		return allOrders;

	});

};

//Retrieve auth user's orders

module.exports.userOrderList = (reqParams) => {

	return User.findById(reqParams.userId).populate('orderedProduct').then(user => {

		return user.orderedProduct;

	});
};



// Retrieve all users by admin

module.exports.getAllUsers = () => {
	return User.find({}).then(result => result)

};