const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.ObjectId;


const userSchema = new mongoose.Schema({
	userName:{
		type: String,
		required: [true, "Username is required"]
	},
	email:{
		type: String,
		required: [true, "Email address is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
	    default: false
	},
	orderedProduct: [
		{
			products:[
				{
					productId: {
					type: ObjectId,
					required: [true, "ProductId is required"]
					},
					productName: {
					type: String,
					required: [true, "ProductId is required"]
					},
					quantity: {
					type: Number,
					required: [true, "Quantity is required"]
					}
				}
			],
			totalAmount: {
				type: Number,

			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			
			
		}
	]



});




module.exports = mongoose.model("User", userSchema);