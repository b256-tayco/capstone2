const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.ObjectId;


const productSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, "Product Name is required"]
	},
	description: {
		type: String,
		required: [true, "Product Description is required"]
	},
	genre: {
		type: String,
		required: [true, "Price is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	
	inStock: {
		type: Boolean,
		default: true 
	},
	createdOn: {
		type: Date,
	    default: new Date()
	},
	userOrders: [
		{
			userId: {
				type: ObjectId,
				required: [true, "User ID is required"]
			},
			orderId: {
				type: String
			}
		}
	]
});


module.exports = mongoose.model("Product", productSchema);
