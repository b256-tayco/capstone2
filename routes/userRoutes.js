const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js")

//User Registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// User Authentication

router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Create Order (non-admin only)


router.post("/checkout", (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		body: req.body
	}
	if(data.isAdmin) {

		res.send(false);

	} else {
					
		userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	}
});




// Retrieve User Details
router.get("/userdetails", (req, res) => {

	userController.getUserDetails().then(resultFromController => res.send(resultFromController));
});



// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

	
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});





//Set user as Admin
router.put("/setadmin/:userId", auth.verify, (req, res) => {

	const info = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin, 
		params: req.params
	}

	if(info.isAdmin) {

		userController.setAsAdmin(info.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}
});

//Retrieve all orders

router.get("/allorders", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin){

		userController.getAllOrders(data.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}

});



//Retrieve auth user's orders
router.get("/:userId/orderlist", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		}

	if(data.isAdmin) {

		res.send(false);

	} else {

	userController.userOrderList(data.params).then(resultFromController => res.send(resultFromController));

	}
});


router.get("/allusers", (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin){

		userController.getAllUsers(data.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}

});


module.exports = router;
