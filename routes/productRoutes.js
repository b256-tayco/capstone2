const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js")

// Create Product
router.post("/add", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {

		productController.addProduct(data.product).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}
});



//Retrieve all products

router.get("/all", (req, res) => {
	//added query so i can pullup all books per genre (fiction and non-fiction) in postman
	let query = req.query;
	if(!query.genre) query = {};
	productController.getAllProducts(query).then(resultFromController => res.send(resultFromController));
});


//Retrive all active (inStock) products

router.get("/stocks", (req, res) => {

	productController.getAllInStock().then(resultFromController => res.send(resultFromController));
});


// Retrive a product
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Update Product info

router.put("/update/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}
});


//Archive product

router.put("/archive/:productId", auth.verify, (req, res) => {

	const data = {
	
		isAdmin: auth.decode(req.headers.authorization).isAdmin, 
		params: req.params
	}

	if(data.isAdmin) {

		productController.archiveProduct(data.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}
});


// Activate product
router.put("/restock/:productId", auth.verify, (req, res) => {

	const data = {
	
		isAdmin: auth.decode(req.headers.authorization).isAdmin, 
		params: req.params
	}

	if(data.isAdmin) {

		productController.restockProduct(data.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);
	}
});



module.exports = router;
